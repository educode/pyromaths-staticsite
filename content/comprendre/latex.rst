:title: LaTeX dans Pyromaths
:date: 2017-12-29 12:00
:category: comprendre
:authors: Jérôme
:description: Modèle LaTeX

Pyromaths crée des fiches d’exercices au format `LaTeX <http://fr.wikipedia.org/wiki/LaTeX>`__.
Ces fichiers *exercices.tex* et *exercices-corrige.tex* ne sont pas imprimables tels quels.
Il faut les compiler pour créer des fichiers **pdf**.

Si vous souhaitez modifier une fiche, il vous faut donc modifier le fichier .tex et le recompiler.
Vous trouverez dans cet article des explications sur les fichiers .tex créés par Pyromaths.

Modules ou paquetages utilisés
==============================

Dans cette partie, je tenterai d’expliquer l’utilité des modules utilisés dans les fichiers .tex.

-  :code:`\documentclass[a4paper,11pt]{article}`
   Toutes les commandes TeX commencent par un :code:`\ `.
   La fonction :code:`documentclass` définit le type du document (ici :code:`article`) et accepte en option
   :code:`a4paper` (le document est au format A4) et :code:`11pt` (c’est la taille des caractères).
-  :code:`\usepackage[latin1]{inputenc}` Cette commande permet d’utiliser des caractères accentués utiles en français.
-  :code:`\usepackage[frenchb]{babel}` Le module :code:`babel` avec l’option :code:`french` “francise” l’utilisation de LaTeX.
   Par exemple, cela permet de gérer convenablement les espaces autour des symboles de ponctuations “:” ou “;”.
-  :code:`\usepackage[fleqn]{amsmath}` :code:`amsmath` est un module qui permet l’édition de formules mathématiques. 
   L’option :code:`fleqn` permet d’aligner les équations à gauches.
-  :code:`\usepackage{amssymb, multicol, calc, vmargin, cancel, fancyhdr, units, pst-eucl, wrapfig, lastpage}`
-  Le module :code:`amssymb` met à disposition tout un tas de symboles, mathématiques ou non 
   (tel le triangle noir qui sert à numéroter les questions dans les exercices).
-  Le module :code:`multicol` permet de créer des colonnes dans une page.
-  Le module :code:`calc` permet de faire des opérations dans un document LaTeX. 
   Il est utilisé pour afficher l’année scolaire en cours par exemple.
-  Le module :code:`vmargin` permet de gérer simplement les marges d’un document LaTeX.
-  Le module :code:`cancel` permet de rayer des caractères avec des barres obliques.
   Il est utilisé pour faire apparaître les simplifications de fractions.
-  Le module :code:`fancyhdr` gère les en-têtes et pieds de pages du document.
-  Le module :code:`units` gère l’affichage des unités de longueur (et d’autres) dans les documents.
-  Le module :code:`pst-eucl` permet de construire des figures géométriques plus simplement en utilisant
   `PSTricks <http://fr.wikipedia.org/wiki/PSTricks>`__.
-  Le module :code:`wrapfig` permet d’écrire du texte autour d’une figure.
-  Le module :code:`lastpage` permet de connaître le numéro de la dernière page.

-  :code:`\setmarginsrb{1.5cm}{1.5cm}{1.5cm}{1.5cm}{.5cm}{.5cm}{.5cm}{1.cm}`
   Cette commande définit dans l’ordre les marges gauche, haut, droite, bas, la hauteur de l’en-tête, 
   la distance entre l’entête et le texte, la hauteur du pied de page et la distance entre le texte 
   et le pied de page.
-  :code:`\newcounter{exo}\setcounter{exo}{1}` Crée un compteur nommé :code:`exo` et l’initialise à 1.
   C’est ce compteur qui numérotera automatiquement les exercices de la feuille.
-  :code:`\setlength{\fboxsep}{1em}` Précise la distance entre le texte et le bord des cadres.
-  :code:`\setlength\parindent{0em}` Fixe la distance pour le retrait de premier paragraphe à 0
   (inappropriée dans une fiche d’exercices).
-  :code:`\setlength\mathindent{0em}` Fixe la distance pour le retrait des formules mathématiques.
-  :code:`\setlength{\columnsep}{30pt}` Fixe la taille de la séparation entre les colonnes.
-  :code:`\usepackage[ps2pdf, pagebackref=true, colorlinks=true, linkcolor=blue, plainpages=true]{hyperref}`
   Permet de créer des hyperliens (référence permettant de passer d’un document consulté à un document lié)
   dans les fichiers pdf.
-  :code:`\hypersetup{pdfauthor={Jérôme Ortais}, pdfsubject={Exercices de mathématiques}, 
   pdftitle={Exercices créés par Pyromaths, un programme en Python de Jérôme Ortais}}`
   Les informations stockées dans le document pdf créé.

Les fonctions utilisées
=======================

Les fichiers d’exercices .tex utilisent des fonctions pour gérer l’affichage des exercices notamment.

Affichage des exercices :
-------------------------

-  :code:`\newenvironment{exercice}[1][ ]{` La commande se nomme :code:`exercice` et peut avoir un argument optionnel 
   (par exemple, le nombre de points de l’exercice dans un devoir).
-  :code:`\pagebreak[2]` Essaie de faire en sorte que le titre de l’exercice ne se retrouve pas seul en bas de page.
-  :code:`\renewcommand{\theenumi}{\arabic{enumi}}` redéfini le type de numérotation (chiffres arabes) 
   des questions dans les exercices.
-  :code:`\renewcommand{\labelenumi}{$\blacktriangleright$\textbf{\theenumi.}}`
   Les questions dans les exercices sont numérotées comme suit : 
   un triangle noir (:code:`$\blacktriangleright$`) suivi du numéro de la question en gras :code:`\textbf{\theenumi.}`).
-  :code:`\renewcommand{\theenumii}{\alph{enumii}}` 
   redéfini le type de numérotation (lettres minuscules) des questions de second niveau dans les exercices.
-  :code:`\renewcommand{\labelenumii}{\textbf{\theenumii)}}` 
   les questions de second niveau dans les exercices sont numérotées comme suit :
   le numéro de la question (en lettre minuscule, souvenez-vous de la ligne précédente) en gras :code:`\textbf{\theenumii.}`).
-  :code:`\begin{flushleft}` le titre de l’exercice sera aligné à gauche.
-  :code:`\fontfamily{pag}\fontseries{b}\\selectfont` 
   il utilisera la police AvantGarde (:code:`pag`) si elle existe, en gras (:code:`\\fontseries{b}`).
-  :code:`\underline{Exercice \theexo{}} - \footnotesize{#1}` 
   écrit “Exercice” suivi du numéro de l’exercice (:code:`\theexo{}`) souligné (:code:`\\underline{ }`). 
   Ajoute au bout de cette ligne un “-” suivi éventuellement du paramètre passé à la commande :code:`\exercice` 
   (les nombres de points donnés à la question par exemple) écrit en plus petit (:code:`\footnotesize{#1}`).
-  :code:`\end{flushleft}` repasse en mode justifié.
-  :code:`\vspace{-2ex}` réduit l’espace vertical de 2ex entre le titre de l’exercice et la ligne suivante

C’est ici que sera écrit le contenu de l’exercice. La suite de la commande permet de gérer la fin de l’exercice.

-  :code:`\vspace{-1ex}` réduit l’espace vertical entre la fin de l’exercice et la suite du document.
-  :code:`\stepcounter{exo}` incrémente la valeur du compteur :code:`exo` qui numérote les exercices.

Calcul de l’année scolaire en cours
-----------------------------------

:code:`\count1=\year \count2=\year \ifnum\month<8\advance\count1by-1 \else\advance\count2by1\fi`

:code:`count1` contiendra l’année de début et :code:`count2` l’année de fin.
La fonction regarde quel est le mois en cours :

-  entre août et décembre, l’année en cours est l’année de début ;
-  entre janvier et juillet, l’année en cours est l’année de fin.

En-tête et pied de page
-----------------------

-  :code:`\pagestyle{fancy}` utilise des en-tête et pieds de page personnalisés
-  :code:`\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}`
   le pied de page central (:code:`\cfoot`) affiche par exemple “année 2007/2008” en italique (:code:`\textsl`)
   et en petit (:code:`\footnotesize`).
-  :code:`\rfoot{\textsl{\tiny{https://www.pyromaths.org}}}` 
   le pied de page droit (:code:`\rfoot`) affiche *https://www.pyromaths.org* en italique (:code:`\textsl`)
   et en encore plus petit (:code:`\tiny`).
-  :code:`\lhead{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}`
   l’en-tête gauche (:code:`\lhead`) affiche par exemple “Page 1/3” en italique (:code:`\textsl`) et en petit (:code:`\footnotesize`).

Plus loin dans le document on trouve les deux commandes suivantes :

-  :code:`\chead{\Large{\textsc{Fiche de préparation au brevet}}}` 
   l’en-tête central (:code:`\chead`) affiche ici “Fiche de préparation au brevet” en grand (:code:`\Large`) 
   et en petites capitales (:code:`\textsc`).
-  :code:`\rhead{\textsl{\footnotesize{Classe de 3\ieme}}}` 
   l’en-tête droit (:code:`\rhead`) affiche ici “Classe de 3e” en petit (:code:`\footnotesize`) et en italique (:code:`\footnotesize`).

