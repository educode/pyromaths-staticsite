:title: Installer à partir des sources
:date: 2009-09-15 12:00
:modified: 2018-07-04 17:09
:category: installer
:authors: Jérôme; Yves
:description: Installer les sources de pyromaths

Voici ce qu’il faut installer sur votre ordinateur pour pouvoir compiler (et donc utiliser) Pyromaths :

* Python version 3.6.x ou plus récent
* PyQt version 5.x ou plus récent
* lxml version 4.2.1 ou plus récent

Il faut ensuite exécuter le fichier `utils/pyromaths-qt`.

Sous Ubuntu
===========
On installe le nécessaire dans un environnement virtuel Python :

.. class:: console

    .. code-block:: bash
        :linenos: none

        sudo aptitude install python3-virtualenv
        virtualenv --no-site-packages ~/pyromaths-sources -p /usr/bin/python3
        cd ~/pyromaths-sources
        source bin/activate
        pip install lxml jinja2 PyQt5 markupsafe sip

Sous Windows
============

Télécharger et installer `Python Windows Installer` sur `www.python.org <http://www.python.org">`_

.. class:: console

  .. code-block:: bat
    :linenos: none

    cd "C:\Users\%username%\"
    python -m venv C:\Users\%username%\pyromaths-sources
    C:\Users\%username%\BUILD-pyromaths-sources\Scripts\pip install --upgrade pip
    C:\Users\%username%\BUILD-pyromaths-sources\Scripts\pip install lxml
    C:\Users\%username%\BUILD-pyromaths-sources\Scripts\pip install markupsafe
    C:\Users\%username%\BUILD-pyromaths-sources\Scripts\pip install jinja2
    C:\Users\%username%\BUILD-pyromaths-sources\Scripts\pip install sip
    C:\Users\%username%\BUILD-pyromaths-sources\Scripts\pip install PyQt5

Sur Mac OS X
=============

Installer Python, Qt, SIP, PyQt et lxml (plus de détails sur la `page dédiée
<developper-une-application-autonome-sur-mac-os-x-109.html>`_ dans la documentation.
