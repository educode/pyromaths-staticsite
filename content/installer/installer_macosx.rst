:title: Installer Pyromaths sous MacOs X
:date: 2009-09-15 12:00
:category: installer
:authors: Yves
:description: Installer Pyromaths sous MacOS X


.. image:: images/pyromaths_mac.png
    :alt: Mac OS X
    :align: center

Configuration requise
=====================


* Ordinateur Mac avec OS X 10.10 ou ultérieur.
* Une distribution LaTeX (nous vous conseillons ***MacTeX*** disponible sur *http://www.tug.org/mactex/*,
  télécharger «MacTeX.pkg» puis procéder à l’installation).

Installation de Pyromaths
=========================

*  Télécharger la version Mac OS X de Pyromaths dans la rubrique **Télécharger** ci-dessus.
*  Glisser-déposer *Pyromaths* dans le dossier *Applications*.

