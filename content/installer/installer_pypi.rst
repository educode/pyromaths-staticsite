:title: Installer Pyromaths depuis Pypi
:category: installer
:date: 2018-07-19
:author: Louis
:description: Installer Pyromaths depuis Pypi

.. note::

   Cette méthode d'installation s'adresse aux utilisateurs expérimentés, qui savent utiliser la ligne de commande.

Pyromaths est `disponible sur Pypi <http://pypi.org/project/pyromaths>`__ ; il est donc possible de l'installer avec ``pip`` (de préférence dans un *virtualenv*) :

* Version en ligne de commande uniquement :

  .. class:: console

      .. code-block:: bash
        :linenos: none

         pip install pyromaths

  Pyromaths peut alors être lancé en utilisant la commande ``pyromaths``.

* Client QT (la version en ligne de commande est automatiquement installée comme dépendances) :

  .. class:: console

      .. code-block:: bash
        :linenos: none

         pip install pyromaths-qt

  Pyromaths peut alors être lancé en utilisant la commande ``pyromaths-qt`` (pour le client QT), ou ``pyromaths`` (pour la version en ligne de commande).
