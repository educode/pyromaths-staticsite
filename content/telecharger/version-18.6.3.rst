:title: Version 18.6.3
:slug: version-18-6-3
:date: 2018-06-26 14:34
:category: telecharger
:description: Liens vers la version 18.6.3

* |debian| `Pyromaths pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths_18.6.3-1_all.deb>`_
* |redhat| `Pyromaths pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-18.6.3-1.noarch.rpm>`_
* |macos| `Pyromaths pour Mac OS X <https://www.pyromaths.org/downloads/pyromaths-18.6.3-macos.dmg>`_
* |windows| `Pyromaths pour Windows <https://www.pyromaths.org/downloads/pyromaths-18.6.3-win32.exe>`_
* |sources| `Sources de Pyromaths <https://www.pyromaths.org/downloads/pyromaths-18.6.3-sources.tar.bz2>`_

.. |debian| image:: images/debian.png
    :alt: Debian Linux
.. |redhat| image:: images/redhat.png
    :alt: RedHat Linux
.. |macos| image:: images/macosx.png
    :alt: Mac OS X
.. |windows| image:: images/winvista.png
    :alt: Windows
.. |sources| image:: images/source.png
    :alt: Sources


Nouveautés de cette version :
=============================
Cette version ne fait que corriger un bug dans Sd2aRacines (Racines d'un polynôme de degré 2 en 1re S)
