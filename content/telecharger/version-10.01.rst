:title: Version 10.01
:slug: version-10.01
:date: 2010-04-24 12:00
:category: telecharger
:description: Liens vers la version 10.01

* |debian| `Pyromaths pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths_10.01_all.deb>`_
* |redhat| `Pyromaths pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-10.01-2.noarch.rpm>`_
* |macos| `Pyromaths pour Mac OS X <https://www.pyromaths.org/downloads/pyromaths-10.01-macos.dmg>`_
* |windows| `Pyromaths pour Windows <https://www.pyromaths.org/downloads/pyromaths-10.01-win32.exe>`_
* |sources| `Sources de Pyromaths <https://www.pyromaths.org/downloads/pyromaths-10.01-sources.tar.bz2>`_

.. |debian| image:: images/debian.png
  :alt: Debian Linux
.. |redhat| image:: images/redhat.png
  :alt: RedHat Linux
.. |macos| image:: images/macosx.png
  :alt: Mac OS X
.. |windows| image:: images/winvista.png
  :alt: Windows
.. |sources| image:: images/source.png
  :alt: Sources

Nouveautés de cette version :
_____________________________

* Un exercice sur les probabilités niveau 3e - par Guillaume Barthélémy
* Modification de l'exercice sur le théorème de Pythagore, niveau 4e :

  - la figure n'est plus dessinée
  - deux questions dans le même exercice : un calcul de l'hypoténuse et un calcul d'un côté de l'angle droit

* Correction d'une erreur dans le corrigé de l'exercice "Écrire un nombre décimal" - merci à Samuel Coupey pour avoir signalé l'erreur
* Correction d'une erreur dans le corrigé de l'exercice "Fonctions affines" - merci à Nicolas Bissonnier pour avoir signalé l'erreur
* Passage à Python 2.6

  - Gestion des caractères accentués dans les noms de fichiers
  - Passage à l'utf-8 pour l'encodage des fichiers.

* Nouvelles fonctions et classes pour une utilisation prochaine dans de nouveaux exercices
