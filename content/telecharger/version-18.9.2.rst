:title: Version 18.9.2
:slug: version-18-9-2
:date: 2018-09-04 22:57
:category: telecharger
:description: Liens vers la version 18.9.2

* |debian| `Pyromaths pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths_18.9.2-1_all.deb>`_ et `Pyromaths-qt pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths-qt_18.9.3-1_all.deb>`_
* |redhat| `Pyromaths pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-18.9.2-1.noarch.rpm>`_ et `Pyromaths-qt pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-qt-18.9.3-1.noarch.rpm>`_
* |macos| `Pyromaths pour Mac Os X <https://www.pyromaths.org/downloads/pyromaths-18.9.3-macos.dmg>`_
* |windows| `Pyromaths pour Windows <https://www.pyromaths.org/downloads/Pyromaths-QT_18.9.2.exe>`_
* |sources| `Sources de Pyromaths <https://pypi.org/project/pyromaths/>`_

.. |debian| image:: images/debian.png
    :alt: Debian Linux
.. |redhat| image:: images/redhat.png
    :alt: RedHat Linux
.. |macos| image:: images/macosx.png
    :alt: Mac OS X
.. |windows| image:: images/winvista.png
    :alt: Windows
.. |sources| image:: images/source.png
    :alt: Sources

Nouveautés de cette version :
=============================

* Bugfix : exercice « Opérations » en 6e.
