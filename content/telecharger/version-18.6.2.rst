:title: Version 18.6.2
:slug: version-18-6-2
:date: 2018-06-18 12:00
:category: telecharger
:link: 
:description: Liens vers la version 18.6.2

* |debian| `Pyromaths pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths_18.6.2-1_all.deb>`_
* |redhat| `Pyromaths pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-18.6.2-1.noarch.rpm>`_
* |macos| `Pyromaths pour Mac OS X <https://www.pyromaths.org/downloads/pyromaths-18.6.2-macos.dmg>`_
* |windows| `Pyromaths pour Windows <https://www.pyromaths.org/downloads/pyromaths-18.6.2-win32.exe>`_
* |sources| `Sources de Pyromaths <https://www.pyromaths.org/downloads/pyromaths-18.6.2-sources.tar.bz2>`_

.. |debian| image:: images/debian.png
    :alt: Debian Linux
.. |redhat| image:: images/redhat.png
    :alt: RedHat Linux
.. |macos| image:: images/macosx.png
    :alt: Mac OS X
.. |windows| image:: images/winvista.png
    :alt: Windows
.. |sources| image:: images/source.png
    :alt: Sources


Nouveautés de cette version :
_____________________________


* Nouvel exercice niveau terminale ES (spécialité) : Déterminer l'état stable d’un graphe probabiliste (en utilisant un système d'équations).
* Nouvel exercice niveau terminale ES (spécialité) : Résoudre un système d'équations en utilisant les matrices.
* Nouvel exercice niveau seconde : Bilan sur les trinômes.
* Création d’une nouvelle classe d’exercices (rétro-compatible), pour faciliter l'écriture d’exercices en utilisant le moteur de templates jinja2.
* Documentation : Ajout d’une page d’exemples.
* Documentation : Ajout d’un tutoriel pour la création de nouveaux exercices.
* Mise à jour de `cree-vignettes.py` (utilisé pour créer les vignettes des exercices).
* Nettoyage du code ; petites corrections de bug ; améliorations diverses.

Merci à Louis pour son travail. Il est à l’origine de l’ensemble des nouveautés.

