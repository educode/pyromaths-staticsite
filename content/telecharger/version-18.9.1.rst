:title: Version 18.9.1
:slug: version-18-9-1
:date: 2018-09-03 16:04
:category: telecharger
:description: Liens vers la version 18.9.1

* |debian| `Pyromaths pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths_18.9.1-1_all.deb>`_ et `Pyromaths-qt pour Linux - deb <https://www.pyromaths.org/downloads/pyromaths-qt_18.9.3-1_all.deb>`_
* |redhat| `Pyromaths pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-18.9.1-1.noarch.rpm>`_ et `Pyromaths-qt pour Linux - rpm <https://www.pyromaths.org/downloads/pyromaths-qt-18.9.3-1.noarch.rpm>`_
* |macos| Bientôt disponibe
* |windows| `Pyromaths pour Windows <https://www.pyromaths.org/downloads/Pyromaths-QT_18.9.2.exe>`_
* |sources| `Sources de Pyromaths <https://pypi.org/project/pyromaths/>`_

.. |debian| image:: images/debian.png
    :alt: Debian Linux
.. |redhat| image:: images/redhat.png
    :alt: RedHat Linux
.. |macos| image:: images/macosx.png
    :alt: Mac OS X
.. |windows| image:: images/winvista.png
    :alt: Windows
.. |sources| image:: images/source.png
    :alt: Sources

Nouveautés de cette version :
=============================

Cette version ne propose aucun exercice nouveau, mais propose de nombreuses améliorations dans la structure interne du code. Elles ne sont pas visibles pour l'utilisateur final, mais devraient faciliter le travail des auteurs, et le développement de nouveaux exercices.

* Séparation de pyromaths en deux projets : pyromaths (client en ligne de commande) et client QT.
* Passage à Python3 (et nombreux changements dans l'API interne, qui n'est pas rétro-compatible).
* Tags

  - Possibilité d'ajouter des tags à des exercices
  - Le niveau d'un exercice n'est plus défini par une chaîne, mais par une liste de tags (un exercice peut donc avoir plusieurs niveaux).

* Binaire, et sous commandes.

  - Pyromaths peut être appelé comme un module python3 : `python3 -m pyromaths`.
  - `ls` :

    + Ajout des options \\-\\-tags et \\-\\-desc, pour filtrer les exercices.

  - `tags` :

    + Création d'une commande `pyromaths tags` permettant d'afficher la liste de tous les tags.

  - `generate` :

    + Ajout d'une option \\-\\-format, permettant de choisir le format de sortie (LaTeX, pdf, latexmkrc).

* Exercices

  - InterpolationMatrices : Correction du choix des coefficients (ils étaint parfois égaux à 0, ce qui provoquait des écritures comme $f(x)=0x²+2x+0$ ; ils peuvent maintenant être décimaux (c'était normalement déjà possible, mais un bug interdisait ce cas).
