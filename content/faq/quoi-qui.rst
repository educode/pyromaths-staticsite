:title: Pyromaths, c'est quoi, c'est qui ?
:date: 2010-05-14 12:00
:modified: 2018-07-08 17:23
:category: faq
:authors: Jérôme
:description: Pyromaths, c'est quoi, c'est qui ?

qu’est-ce ?
===========

Pyromaths est un programme initié par Jérôme Ortais, qui a pour but de créer des exercices type de mathématiques niveau
collège et lycée ainsi que leur corrigé.
C’est ce qu’on appelle parfois un exerciseur.
Contrairement à de nombreux autres projets, Pyromaths a pour objectif de proposer une correction véritablement détaillée
des exercices proposés et pas seulement une solution.

Il permet par exemple de proposer des devoirs maison aux élèves et de leur distribuer ensuite la correction.
Il peut aussi servir à des familles afin qu’un élève puisse travailler un point du programme et se corriger ensuite.

historique
==========

L’idée m’est venue en 2006.
Un ami avait créé des macros sous un tableur propriétaire pour faire travailler ses 3e sur le calcul fractionnaire,
les puissances de 10, …
L’idée était que la vie scolaire puisse bénéficier d’un stock de fiches de travail pour animer les permanences.
Étant un fervent utilisateur du logiciel libre et passionné de programmation, j’ai commencé par refaire le même travail
sous OpenOffice Calc.
Mais je n’étais pas satisfait du rendu.
Parallèlement, je collaborais au projet Dmaths.
Il m’a donc semblé naturel de créer des macros pour OpenOffice Writer et d’utiliser le rendu mathématique proposé par openOffice.
J’ai alors rencontré deux problèmes :

-  les documents contenant plusieurs pages de formules étaient pénibles à manipuler car trop lourds pour ma machine ;
-  je n’étais pas convaincu par le rendu des fiches et je ne pouvais pas les intégrer simplement dans mes cours rédigés
   en LaTeX, qui me semble encore supérieur à tout traitement de texte pour tout ce qui est écriture d’articles scientifiques.

J’ai donc décidé de travailler sur des fichiers LaTeX, avec tous les avantages que propose ce programme.
Il me fallait un langage de programmation libre et multi-plateforme.
Je voulais que mon programme fonctionne sous Linux car c’est le système d’exploitation que j’utilise, mais aussi sous
Windows pour que le plus grand nombre puisse en profiter.
Mon choix s’est porté sur Python et wx-Python pour l’interface graphique.
C’est ainsi qu’est né Pyromaths.

Débuter un projet est un gros travail.
Il faut créer un site Internet, faire des tests sur plusieurs systèmes d’exploitation, créer des installateurs pour chacun d’eux…
C’est une période où le programme en lui-même est relégué à un second plan et où le découragement guète.
J’ai heureusement eu la chance d’obtenir de l’aide de JGD et d’Arnaud que j’avais rencontrés lors de ma collaboration à Dmaths.

Depuis, Pyromaths a bien progressé.
Il dispose aujourd’hui de 86 exercices répartis sur les 4 niveaux du collège et le lycée.
Il permet depuis janvier 2008 de créer des fiches directement sur le site Internet
`https://enligne.pyromaths.org <https://enligne.pyromaths.org>`__.
Ceci permet d’éviter d’avoir à installer une distribution LaTeX sur son ordinateur si on n’est pas à l’aise avec
l’informatique et qu’on ne s’en sert pas par ailleurs.

Un dépôt apt pour les distributions Linux basées sur Debian permet d’installer et de garder à jour Pyromaths sans effort.

Le code source se trouve sur le serveur de `framagit <https://framagit.org/pyromaths/pyromaths>`__ et permet à présent un
travail collaboratif sur le développement de Pyromaths et nous sommes actuellement quelques-un à regarder de près le code.
Ce projet progresse donc assez rapidement.

Le retour des utilisateurs est sans doute ce qui est le plus motivant dans un projet comme celui-ci.
Et chaque retour positif est toujours un encouragement à poursuivre le projet.

qui sont-ils ?
==============

Je tiens à remercier les personnes suivantes qui contribuent ou on contribué au développement de Pyromaths :

-  **Jacqueline Gouguenheim-Desloy** a été la première à aider au développement de Pyromaths.
   Elle est à l’origine du portage sur Mac OS X.
   Son soutien et son amitié nous ont été précieux.
   Sa disparition est une perte douloureuse pour la communauté du logiciel libre.
-  **Arnaud Kientz** a beaucoup travaillé sur le visuel du site Pyromaths à ses débuts et sur l’interface graphique du logiciel.
   Il propose maintenant aussi des exercices.
-  **Yves Gesnel** est l’actuel responsable du portage de Pyromaths sous Mac OS X.
   Il a aussi beaucoup aidé à la lisibilité du projet sur l’Internet et c’est grâce à lui que Pyromaths sera proposé dans Ubuntu.
   Il propose également des exercices.
-  **Guillaume Barthélémy** a produit de nombreux exercices.
-  **Nicolas Bissonnier** a également produit de nombreux exercices et est à l’origine de la création de la partie Lycée.
-  **Dider Roche** s’est chargé de l’insertion de Pyromaths dans les dépôts Ubuntu.
-  **Louis Paternaut** a énormément travaillé pour le logiciel.

Merci à vous !
