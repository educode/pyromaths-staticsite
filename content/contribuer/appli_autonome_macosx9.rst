:date: 2014-06-10 12:00
:title: Développer une application autonome sur Mac OS X 10.9
:category: contribuer
:author: Yves
:description: Développer une application autonome sur Mac OS X 10.9

Cet article est destiné aux développeurs et décrit la procédure pour obtenir une application autonome de Pyromaths sur
OS X 10.9 à partir du code source.

Avec PyQt, il est possible de réaliser des programmes multi-plateformes.
Cependant, le programme obtenu est un script Python qui nécessite la présence de Python, de PyQt et d’autres librairies
et ne ressemble donc pas tout à fait à une application native du système.

Avec `py2app <https://pypi.python.org/pypi/py2app/>`__, il est possible d’obtenir une véritable application autonome
sur Mac OS X à partir du programme réalisé avec PyQt.

Environnement de Développement
==============================

py2app n’est malheureusement pas capable de réaliser une application autonome à partir de la version de Python fournie avec OS X.
Il va donc falloir installer un environnement de développement avec une nouvelle version de Python ainsi que PyQt avec les dépendances nécessaires.

**Installation de Python**

`Télécharger <http://www.python.org/>`__ et installer *python-2.7.6-macosx10.6.dmg*.

la distribution Python fournie avec Mac OS X se trouve dans */System/Library/Frameworks/Python.framework/*
alors que celle qu’on vient d’installer se situe dans */Library/Frameworks/Python.framework*.

**Installation de Qt**

`Télécharger <http://qt.digia.com/>`__ et installer *qt-mac-opensource-4.8.6.dmg*.

**Installation de SIP**

`Télécharger <http://www.riverbankcomputing.co.uk/>`__ et décompresser *sip-4.15.5.tar.gz* puis exécuter les commandes :

.. class:: console

  .. code-block:: bash
    :linenos: none

    cd sip-4.15.5
    python configure.py —arch x86_64
    make
    sudo make install

**Installation de PyQt4**

`Télécharger <http://www.riverbankcomputing.co.uk/>`__ et décompresser *PyQt-mac-gpl-4.10.4.tar.gz* puis exécuter les commandes :

.. class:: console

  .. code-block:: bash
    :linenos: none

    cd PyQt-mac-gpl-4.10.4
    python configure.py —use-arch x86_64
    make
    sudo make install

**Installation de lxml**

`Télécharger <http://lxml.de/>`__ et décompresser *lxml-3.3.5.tgz* puis exécuter les commandes:

.. class:: console

  .. code-block:: bash
    :linenos: none

    cd lxml-3.3.5
    python setup.py build —static-deps
    sudo python setup.py install

Maintenant que Qt, SIP, PyQt4 et lxml sont installés, il est possible d’exécuter Pyromaths en lançant le fichier
pyromaths.py depuis l’application Terminal.

Installation de py2app
======================

Les manipulations suivantes sont dédiées à l’installation de py2app.

**Installation des setuptools**

exécuter les commandes:

.. class:: console

  .. code-block:: bash
    :linenos: none

    curl -O http://peak.telecommunity.com/dist/ez_setup.py
    sudo python ez_setup.py -U setuptools

**Installation de py2app**

exécuter la commande:

.. class:: console

  .. code-block:: bash
    :linenos: none

     sudo easy_install -U py2app

**Installation de zlib**

zlib est utilisé par py2app pour la compression du dossier site-packages.

`Télécharger <http://www.zlib.net/>`__ et décompresser *zlib-1.2.8.tar.gz* puis exécuter les commandes:

.. class:: console

  .. code-block:: bash
    :linenos: none

    cd zlib-1.2.8
    ./configure -s
    make
    sudo make install

Réalisation de Pyromaths.app
============================

L’application Pyromaths.app est réalisée avec le programme make qui va exécuter plusieurs actions définies par le Makefile.

Dans un premier temps, la commande python setup.py py2app permet d’obtenir une application autonome.
Pour éviter que les librairies Qt ne soient chargées deux fois, celles du bundle Pyromaths.app et celles du système,
le fichier qt.conf est automatiquement ajouté par py2app dans Pyromaths.app/Contents/Resources/.
Ensuite, en post-processing, pour pouvoir lancer l’application avec les variables d’environnement nécessaires à
l’utilisation de LaTeX, le script shell nommé setenv.sh est ajouté dans Pyromaths.app/Contents/MacOS/.
Ensuite, le CFBundleExecutable du fichier info.plist est modifié pour prendre en compte le script setenv.sh.
L’application dist/Pyromaths.app ainsi obtenue est fonctionnelle.

Dans un deuxième temps, la traduction en français des menus est ajoutée et pour optimiser la taille de notre application,
des fichiers inutiles sont supprimés du bundle Pyromaths.app.
Pour alléger encore l’application obtenue, le code lié à une architecture 32 bits est éliminé.
L’application *dist/Pyromaths-x86_64.app* ainsi obtenue est optimisée.

`Télécharger <http://www.pyromaths.org/>`__ et décompresser les sources de Pyromaths puis exécuter les commandes
suivantes pour obtenir une version optimisée 64 bits:

.. class:: console

  .. code-block:: bash
    :linenos: none

    cd pyromaths
    make app
