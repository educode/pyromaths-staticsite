:title: Les commandes utiles pour Git
:date: 2010-05-03 12:00
:category: contribuer
:authors: Jérôme
:description: Les commandes utiles pour Git

Voici la liste des commandes que j’utilise régulièrement avec Git :

Fonctions de base
=================

- Pour ajouter de nouveaux fichiers au dépôt :

  .. class:: console
  
    .. code-block:: bash
      :linenos: none
  
      git add **fichier**

- Pour appliquer toutes les modifications faites :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git commit -a

  et laisser un message explicatif dans le fichier qui s’ouvre.

- Pour appliquer seulement quelques modifications faites :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git commit **fichiers**

  et laisser un message explicatif dans le fichier qui s’ouvre.

- Pour publier les *commits* sur le serveur :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git repack
      git prune
      git push

- Pour récupérer la dernière version du serveur :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git pull

Manipulation des branches
=========================

- Pour créer une branche python3:

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git branch python3

- Pour publier la branche python3:

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git push origin python3

- Pour créer et sélectionner la branche modeles :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git checkout --track -b modeles origin/modeles

- Pour sélectionner la branche modeles :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git checkout modeles

- Pour supprimer la branche locale modeles :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git branch -d modeles

- Pour supprimer la branche distante modeles :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git push origin :modeles

En cas de mauvaises manipulations
=================================

- Pour annuler les changements effectués sur un fichier local :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git checkout **fichier**

- Pour annuler **toutes** les modifications faites en local :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git reset --hard origin/develop

  ou pour la branche modeles :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git reset --hard origin/modeles

Étiqueter une version (pour Jérôme seulement)
=============================================

- Pour ajouter un tag (lors de la sortie d’une version)

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git log --pretty=format:‘%h : %s’ --graph

  pour repérer le nom SHA1 du commit à tagger en options

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git tag -u B39EE5B6 version-09.
      08 795e2b0

  pour tagguer le commit 795e2b0

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git push --tags

- Pour supprimer le tag *version-09.08* mal placé :

  .. class:: console

    .. code-block:: bash
      :linenos: none

      git tag -d version-09.08
      git push origin :refs/tags/version-09.08
