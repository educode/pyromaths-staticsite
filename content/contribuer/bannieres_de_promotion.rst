:title: Bannières de promotion
:slug: bannieres_de_promotion
:date: 2010-05-14 12:00
:category: contribuer
:link:
:author: Yves
:description: Faites de la publicité pour Pyromaths

Vous pouvez télécharger une bannière et l'héberger sur votre site web ou bien copier-coller le code correspondant suivant la bannière :

|120x120|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="https://www.pyromaths.org/"><img src="https://pyromaths.org/static/img/bannieres/pyromaths120.png" width="120" height="120" alt="pyromaths" /></a>

|200x200|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths200.png" width="200" height="200" alt="pyromaths" /></a>

|250x250|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths250.png" width="250" height="250" alt="pyromaths" /></a>

|300x300|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths120.png" width="300" height="300" alt="pyromaths" /></a>

|468x60|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths468.png" width="468" height="60" alt="pyromaths" /></a>

|728x90|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths728.png" width="728" height="90" alt="pyromaths" /></a>

|120x600|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths120par600.png" width="120" height="600" alt="pyromaths" /></a>

|160x600|

.. class:: code

    .. code-block:: html
        :nowrap:

        <a href="http://www.pyromaths.org/"><img src="http://pyromaths.org/static/img/bannieres/pyromaths160par600.png" width="160" height="600" alt="pyromaths" /></a>

.. |120x120| image:: images/bannieres/pyromaths120.png
    :alt: pyromaths 120x120
.. |200x200| image:: images/bannieres/pyromaths200.png
    :alt: pyromaths 200x200
.. |250x250| image:: images/bannieres/pyromaths250.png
    :alt: pyromaths 250x250
.. |300x300| image:: images/bannieres/pyromaths300.png
    :alt: pyromaths 300x300
.. |468x60| image:: images/bannieres/pyromaths468.png
    :alt: pyromaths 468x60
.. |728x90| image:: images/bannieres/pyromaths728.png
    :alt: pyromaths 728x90
.. |120x600| image:: images/bannieres/pyromaths120par600.png
    :alt: pyromaths 120x600
.. |160x600| image:: images/bannieres/pyromaths160par600.png
    :alt: pyromaths 160x600
