:title: Réaliser un modèle de document
:date: 2009-09-16
:category: contribuer
:authors: Arnaud, Louis
:description: Réaliser un modèle de document

Réaliser son propre modèle de document permet de personnaliser l'apparence de la fiche d'exercice (pour y ajouter son nom, celui de son collège ou lycée, sa classe, pour afficher le corrigé de chaque exercice à la suite de l'énoncé, etc.).

Ceci est documenté sur la `documentation <http://doc.pyromaths.org/fr/master/modele/>`_.
