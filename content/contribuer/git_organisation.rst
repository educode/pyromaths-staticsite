:title: Comment est organisé le dépôt Git
:date: 2010-05-26 12:00
:category: contribuer
:authors: Jérôme
:description: Comment est organisé le dépôt Git

Cet article est très nettement inspiré de `cet article <https://nvie.com/posts/a-successful-git-branching-model/>`__.

La raison de ce changement de stratégie est la suivante : si un bug est découvert,
il n’y a pas moyen de récupérer le code source corrigé sur le dépôt Git si de nouvelles fonctionnalités ont été ajoutées entre temps.
De même, si le développeur 1 travaille sur un exercice 1 et le développeur 2 sur un exercice 2,
il n’est pas possible de sortir une version ne contenant que l’exercice 1 si l’exercice 2 n’est pas fini.

Git permet de gérer les branches. Cela est très efficace dans ce genre de situation.
Voici comment il convient de procéder.

La branche master
=================

Personne ne devrait travailler sur la branche *master*.
Celle-ci ne sert qu’à publier les versions.
Donc la branche *master* devrait toujours refléter la dernière version de Pyromaths.

La branche develop
==================

Cette branche contient les nouveautés abouties.
Ce sont celles qui seront incorporées telle quelle dans la branche *master* lors de la sortie d’une nouvelle version.

Créer une branche pour une nouvelle fonctionnalité
==================================================

Quand on commence à travailler sur une nouvelle fonctionnalité, on crée une branche pour celle-ci en local :

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout -b aires-6e develop

On se retrouve alors dans cette branche et on peut travailler l’esprit tranquille.

incorporer la nouvelle fonctionnalité dans la branche develop
-------------------------------------------------------------

Une fois le travail fini, on rapatrie le travail dans la branche principale *develop*.

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout develop
      git merge —no-ff aires-6e
      git branch -d aires-6e
      git push origin develop

L’option ``--no-ff`` permet de conserver les différentes étapes du travail dans la branche *develop*.

Les branches release
====================

créer une branche release
-------------------------

Les branches release sont créées à partir de la branche *develop*.
Cela permet de vérifier la présence de bugs et de les corriger avant de véritablement publier une nouvelle version sur le site.
Dans une branche release, on n’ajoute plus de nouveautés.
Celles-ci doivent aller dans une branche *develop*.

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout -b release-10.06 develop
      ./scripts/nouvelle-version.sh
      git commit -a -m “Change les numéros de version pour 10.06”

Ici, on crée une nouvelle branche release-10.06 à partir de la branche  *develop* et on y bascule.

Le script nouvelle-version.sh change les numéros de version dans les fichiers configuration (pyromaths, py2exe, py2app, py2deb).

Enfin, on publie les modifications.

mettre fin à une branche release
--------------------------------

Une fois qu’une branche release semble suffisamment stable pour être publiée,
il faut basculer son contenu dans la branche *master* afin de publier véritablement une nouvelle version et y mettre un
tag afin de savoir de quelle version il s’agit.

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout master
      git merge —no-ff release-10.06
      git tag -u B39EE5B6 10.06

Il faut ensuite rapatrier ces changements dans la branche *develop* afin que celle-ci contienne bien les dernières
nouveautés de la branche *master* (notamment en cas de résolution de bugs dans la branche release).

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout develop
      git merge —no-ff release-10.06

Cela provoquera sans doute des conflits (notamment à cause du changement de numéro de version dans les fichiers de config).
Il suffit de les régler.

Enfin, on peut supprimer la branche release puisqu’elle a été intégrée.

.. class:: console

  .. code-block:: bash
    :linenos: none

      git branch -d release-10.06

Les branches bugfix
===================

Ces branches ont pour but de régler un bug le plus rapidement possible pour publier une nouvelle version.

créer une branche bugfix
------------------------

On crée un telle branche à partir de la branche *master*.
En supposant que la version 10.06 contienne un bug, on va alors créer une branche bugfix-10.06-1 :

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout -b bugfix-10.06-1 master
      ./scripts/nouvelle-version.sh
      git commit -a -m "Change les numéros de version pour 10.06-1"

Ensuite, on corrige le bug et on publie les changement dans la branche :

.. class:: console

  .. code-block:: bash
    :linenos: none

    git commit -m "Règle le problème de virgule dans l’exercice décimaux niveau sixième"

mettre fin à une branche bugfix
-------------------------------

Une fois que les bugs sont corrigés, il faut intégrer les changements à la fois dans *master* et dans *develop*.

Pour mettre à jour la branche *master* :

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout master
      git merge —no-ff bugfix-10.06-1
      git tag -u B39EE5B6 10.06-1

Pour mettre à jour la branche *develop* :

.. class:: console

  .. code-block:: bash
    :linenos: none

      git checkout develop
      git merge —no-ff bugfix-10.06-1

Enfin, on supprime la branche bugfix :

.. class:: console

  .. code-block:: bash
    :linenos: none

      git branch -d bugfix-10.06-1

Conclusion
==========

J’espère que tout ceci ne vous paraîtra pas trop lourd.
Cela permet à chacun de travailler sur Pyromaths sans que nous nous marchions sur les pieds tout en gardant à chaque
fois une version “propre” du logiciel.