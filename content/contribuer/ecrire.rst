:title: Écrire un nouvel exercice
:date: 2018-07-19
:category: contribuer
:author: Louis
:description: Écrire un nouvel exercice

Un tutoriel `est disponible <http://doc.pyromaths.org/fr/master/ecrire/>`__ pour expliquer comment écrire un nouvel exercice pour Pyromaths.
